cwlVersion: v1.0

#Requirements
# - Star Indexes
# - Gencode/GTF File
# - Chromosome Len File

class: Workflow
label: DAPHNI RNA+DNA Pipeline
requirements:
  InlineJavascriptRequirement: {}
  MultipleInputFeatureRequirement: {}
  StepInputExpressionRequirement: {}
  ScatterFeatureRequirement: {}
  SubworkflowFeatureRequirement: {}


inputs:
  NormalDNAForwardReads: File[]
  NormalDNAReverseReads: File[]
  TumorDNAForwardReads: File[]
  TumorDNAReverseReads: File[]
  TumorRNAForwardReads: File[]
  TumorRNAReverseReads: File[]
  BWA-Index: File
  MaxThreads: int
  KnownSiteVCF: File
  AssemblyFasta: File
  BlackList: File
  STARIndex: Directory
  STARGtf: File
  LenFile: File
  IntervalFile: File
  GenomicRegion: string[]
  RefAnnotationFile: File
  FCCohortFolder: Directory
  GeneConversionTable: File


outputs:
  normal_dna_Final_Bam:
    type: File
    outputSource: normal_dna/Final_Bam
  normal_dna_Final_Bam_Index:
    type: File
    outputSource: normal_dna/Final_Bam_Index
  normal_dna_MarkDuplicates_Metrics:
    type: File
    outputSource: normal_dna/MarkDuplicates_Metrics

  tumor_dna_Final_Bam:
    type: File
    outputSource: tumor_dna/Final_Bam
  tumor_dna_Final_Bam_Index:
    type: File
    outputSource: tumor_dna/Final_Bam_Index
  tumor_dna_MarkDuplicates_Metrics:
    type: File
    outputSource: tumor_dna/MarkDuplicates_Metrics

  tumor_rna_Final_Bam:
    type: File
    outputSource: tumor_rna_pre/Final_Bam
  tumor_rna_Final_Index:
    type: File
    outputSource: tumor_rna_pre/Final_Index
  tumor_rna_MarkDuplicates_Metrics:
    type: File
    outputSource: tumor_rna_pre/MarkDuplicates_Metrics
  tumor_rna_Unmapped_Reads1:
    type: File
    outputSource: tumor_rna_pre/Unmapped_Reads1
  tumor_rna_Unmapped_Reads2:
    type: File
    outputSource: tumor_rna_pre/Unmapped_Reads2
  tumor_rna_Fusions:
    type: File
    outputSource: tumor_rna_pre/Fusions
  consensus_variant_calls:
    type: File
    outputSource: variant_calling/consensus_vcf
  counted_vcf:
    type: File
    outputSource: variant_calling/counted_vcf

  tumor_rna_features:
    type: File
    outputSource: tumor_rna_post/features



steps:
  normal_dna:
    run: https://gitlab.com/iidsgt/biocwl/raw/b56e7eefc6b592c9ba088bef448b700f6c789505/Workflows/DAPHNI-DNA.cwl
    in:
      ForwardReads: NormalDNAForwardReads
      ReverseReads: NormalDNAReverseReads
      BWA-Index: BWA-Index
      MaxThreads: MaxThreads
      SampleID:
        valueFrom: "normal"
      KnownSiteVCF: KnownSiteVCF
    out: [Final_Bam, Final_Bam_Index, MarkDuplicates_Metrics]

  tumor_dna:
    run: https://gitlab.com/iidsgt/biocwl/raw/b56e7eefc6b592c9ba088bef448b700f6c789505/Workflows/DAPHNI-DNA.cwl
    in:
      ForwardReads: TumorDNAForwardReads
      ReverseReads: TumorDNAReverseReads
      BWA-Index: BWA-Index
      MaxThreads: MaxThreads
      SampleID:
        valueFrom: "tumor"
      KnownSiteVCF: KnownSiteVCF
    out: [Final_Bam, Final_Bam_Index, MarkDuplicates_Metrics]

  tumor_rna_pre:
    run: https://gitlab.com/iidsgt/biocwl/raw/b56e7eefc6b592c9ba088bef448b700f6c789505/Workflows/DAPHNI-RNA-Pre.cwl
    in:
      ForwardReads: TumorRNAForwardReads
      ReverseReads: TumorRNAReverseReads
      Threads: MaxThreads
      STARIndex: STARIndex
      STARGtf: STARGtf
      LenFile: LenFile
      AssemblyFasta: AssemblyFasta
      BlackList: BlackList
    out: [Final_Bam, Final_Index, MarkDuplicates_Metrics, Unmapped_Reads1, Unmapped_Reads2,
      Fusions, FusionsDiscarded]

  tumor_rna_post:
    run: https://gitlab.com/iidsgt/biocwl/raw/b56e7eefc6b592c9ba088bef448b700f6c789505/Workflows/DAPHNI-RNA-Post.cwl
    in:
      InputBam: tumor_rna_pre/Final_Bam
      Threads: MaxThreads
      Annotation: RefAnnotationFile
    out: [features]

  variant_calling:
    run: https://gitlab.com/iidsgt/biocwl/raw/68534e6ed3059c1a7a5f249bbac3167110e1f269/Workflows/DAPHNI-Consenus-Variant.cwl
    in:
      TumorBam: tumor_dna/Final_Bam
      NormalBam: normal_dna/Final_Bam
      Threads: MaxThreads
      Reference: BWA-Index
      IntervalFile: IntervalFile
      GenomicRegion: GenomicRegion
    out: [consensus_vcf, counted_vcf]

  Sam2Fastp:
    run: https://gitlab.com/iidsgt/biocwl/raw/26d69f74b7f7531f16758c6c781eed43c91a7f98/Tools/Samtools/Samtools-Fastp.cwl
    in:
      InputFile: tumor_rna_pre/Final_Bam
    out: [html, json]