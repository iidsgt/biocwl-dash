FROM centos/systemd

# Install system dependencies
RUN yum install -y epel-release
RUN yum install -y centos-release-scl

# Follow instructions for installing "Inline with Upstream Stable" [IUS]:
# https://www.2daygeek.com/install-enable-ius-community-repository-on-rhel-centos/
RUN curl 'https://setup.ius.io/' -o setup-ius.sh
RUN sh setup-ius.sh

RUN yum install -y gcc
RUN yum install -y python36u
RUN yum install -y python36u-pip
RUN yum install -y python36u-devel
RUN yum install -y nginx
RUN yum install -y pcre pcre-devel
RUN yum install -y git
RUN yum install -y poppler-utils

WORKDIR /biocwl-dash
COPY . .
RUN pip3.6 install -e .
# RUN pip3 install biocwl-dash
COPY example_report/ example_report/
COPY test/ test/

EXPOSE 5000

ENTRYPOINT ["python3.6", "test/test_main.py"]